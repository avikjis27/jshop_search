/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jsearch.bo;

import java.util.ArrayList;

/**
 *
 * @author IBM_ADMIN
 */
public class SearchResult {
    String type;
    String subType;
    ArrayList results;

    public String getType() {
        return type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList getResults() {
        return results;
    }

    public void setResults(ArrayList results) {
        this.results = results;
    }
    
    
    
}
