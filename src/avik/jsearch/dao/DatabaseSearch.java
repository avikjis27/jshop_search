/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jsearch.dao;

import avik.jsearch.bo.SearchResult;
import java.util.ArrayList;
import org.hibernate.Query;
import org.hibernate.Session;
import persistency.HibernateActivity;

/**
 *
 * @author IBM_ADMIN
 */
public class DatabaseSearch {
    
    public static SearchResult[] searchCustomer(String hint)
    {
        String queryString;
        String[] type;
        String[] splittedString;
        splittedString=hint.split("~");
        SearchResult[] searchResultsArray;
        
        Query query=null;
        if(splittedString.length==1 || splittedString.length==2)
             searchResultsArray = new SearchResult[1];
        else
            searchResultsArray = new SearchResult[splittedString.length-1];
        
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        if(splittedString.length==1)
        {
            SearchResult sr= new SearchResult();
            sr.setSubType("NAME");
            sr.setType("CUSTOMER");
                
                query=session.createQuery(" from CustomerDetails a where  a.customerName like :name ");
                   /* + "a.customerAddress like :address or "
                    + "a.contact like :contact or "
                    + "a.city like :city or "
                    + "a.state like :state");*/
                query.setString("name", "%"+splittedString[0].toUpperCase()+"%");
                ArrayList list=(ArrayList) query.list();
                if(list.isEmpty()) {
                   
                }
                else{
                    sr.setResults(list);
                    searchResultsArray[0]=sr;
                }
        }
        
        else
        {
            for(int i=0; i<searchResultsArray.length ; i++)
            {   

                //type[i-1]=splittedString[i];
                if(splittedString[i+1].equalsIgnoreCase("NAME"))
                {
                    SearchResult sr= new SearchResult();
                    sr.setSubType("NAME");
                    sr.setType("CUSTOMER");

                    query=session.createQuery(" from CustomerDetails a where  a.customerName like :name ");
                       /* + "a.customerAddress like :address or "
                        + "a.contact like :contact or "
                        + "a.city like :city or "
                        + "a.state like :state");*/
                    query.setString("name", "%"+splittedString[0].toUpperCase()+"%");
                    ArrayList list=(ArrayList) query.list();

                    if(list.isEmpty()) {

                    }
                    else{
                        sr.setResults(list);
                        searchResultsArray[i]=sr;
                    }



                }
                if(splittedString[i+1].equalsIgnoreCase("ADDRESS"))
                {
                    SearchResult sr= new SearchResult();
                    sr.setSubType("ADDRESS");
                    sr.setType("CUSTOMER");

                    query=session.createQuery(" from CustomerDetails a where  a.customerAddress like :address ");
                    query.setString("address", "%"+splittedString[0].toUpperCase()+"%");
                    ArrayList list=(ArrayList) query.list();
                    if(list.isEmpty()) {

                    }
                    else{
                        sr.setResults(list);
                        searchResultsArray[i]=sr;
                    }


                }
                if(splittedString[i+1].equalsIgnoreCase("CITY"))
                {
                    SearchResult sr= new SearchResult();
                    sr.setSubType("CITY");
                    sr.setType("CUSTOMER");
                    query=session.createQuery(" from CustomerDetails a where  a.city like :city ");
                    query.setString("city", "%"+splittedString[0].toUpperCase()+"%");
                    ArrayList list=(ArrayList) query.list();
                    if(list.isEmpty()) {

                    }
                    else{
                        sr.setResults(list);
                        searchResultsArray[i]=sr;
                    }

                }
                if(splittedString[i+1].equalsIgnoreCase("STATE"))
                {
                    SearchResult sr= new SearchResult();
                    sr.setSubType("STATE");
                    sr.setType("CUSTOMER");
                    query=session.createQuery(" from CustomerDetails a where  a.state like :state ");
                    query.setString("state", "%"+splittedString[0].toUpperCase()+"%");
                    ArrayList list=(ArrayList) query.list();
                    if(list.isEmpty()) {

                    }
                    else{
                        sr.setResults(list);
                        searchResultsArray[i]=sr;
                    }

                }
                if(splittedString[i+1].equalsIgnoreCase("CONTACT"))
                {
                    SearchResult sr= new SearchResult();
                    sr.setSubType("CONTACT");
                    sr.setType("CUSTOMER");
                    query=session.createQuery(" from CustomerDetails a where  a.contact like :contact ");
                    query.setString("contact",splittedString[0].toUpperCase()+"%");
                    ArrayList list=(ArrayList) query.list();
                    if(list.isEmpty()) {

                    }
                    else{
                        sr.setResults(list);
                        searchResultsArray[i]=sr;
                    }
                }
            }
        }
        System.out.println("Size of result:"+searchResultsArray.length);
        return searchResultsArray;
        
    }
    public static SearchResult[] searchItem(String hint)
    {
        String queryString;
        String[] type=null;
        String[] splittedString;
        splittedString=hint.split("~");
        Query query=null;
        SearchResult[] searchResultsArray;
        if(splittedString.length==1 || splittedString.length==2)
             searchResultsArray = new SearchResult[1];
        else
            searchResultsArray = new SearchResult[splittedString.length-1];
        
        
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        if(splittedString.length==1)
        {
            SearchResult sr= new SearchResult();
            sr.setSubType("SERIAL");
            sr.setType("ITEM");
                
                
                
                query=session.createQuery("from ItemDetails a where upper(a.itemId) like :ShortId");
                query.setString("ShortId", "%"+splittedString[0].toUpperCase()+"%");
                ArrayList list=(ArrayList) query.list();
        
                if(list.isEmpty()) {
                   
                }
                else{
                    sr.setResults(list);
                    searchResultsArray[0]=sr;
                }
        }
        else{
        
        for(int i=0; i<searchResultsArray.length ; i++)
        {   
            
            
            if(splittedString[i+1].equalsIgnoreCase("SERIAL"))
            {
                SearchResult sr= new SearchResult();
                sr.setSubType("SERIAL");
                sr.setType("ITEM");
                
                query=session.createQuery("from ItemDetails a where upper(a.itemId) like :ShortId");
                query.setString("ShortId", "%"+splittedString[0].toUpperCase()+"%");
                ArrayList list=(ArrayList) query.list();
        
                if(list.isEmpty()) {
                   
                }
                else{
                    sr.setResults(list);
                    searchResultsArray[i]=sr;
                }
                    
                
                
            }
            
            if(splittedString[i+1].equalsIgnoreCase("MODEL"))
            {
                SearchResult sr= new SearchResult();
                sr.setSubType("MODEL");
                sr.setType("ITEM");
                
                query=session.createQuery("from ItemDetails a where upper(a.itemModelNumber) like :modelNumber");
                query.setString("modelNumber", "%"+splittedString[0].toUpperCase()+"%");
                ArrayList list=(ArrayList) query.list();
        
                if(list.isEmpty()) {
                   
                }
                else{
                    sr.setResults(list);
                    searchResultsArray[i]=sr;
                }
                    
                
                
            }
        }
        }
        /***************************/
        return searchResultsArray;
        
        
    }
    
}
