/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package avik.jsearch.model;

import avik.jsearch.bo.SearchResult;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import persistency.CustomerMaster;
import persistency.ItemMaster;

/**
 *
 * @author IBM_ADMIN
 */
public class SearchResultPanelModel {
    
    public static void updateResltPane(SearchResult[] searchResults, JEditorPane jEditorPane)
    {   for(int j=0;j<searchResults.length;j++){
        SearchResult sr=searchResults[j];
        if(sr==null)
            continue;
        if(sr.getResults()!=null && sr.getType().equals("CUSTOMER"))
        {   jEditorPane.setText(jEditorPane.getText()+"Details showing of "+sr.getType()+" For matching as "+sr.getSubType()+"\n");
            for(int i=0;i<sr.getResults().size();i++)
            {
                CustomerMaster cd=(CustomerMaster) sr.getResults().get(i);
                
                    jEditorPane.setText(jEditorPane.getText()+(i+1)+". "+cd.getCustomerName()+"\n\t"
                            +"Address: "+cd.getCustomerAddress()+"\n\t"
                            +"City: "+cd.getCity()+"\n\t"
                            +"State: "+cd.getState()+"\n\t"
                            +"PIN: "+cd.getPIN()+"\n\t"
                            +"Contact: "+cd.getContact()+"\n");
                    jEditorPane.setText(jEditorPane.getText()+"*****************************************************"+"\n\n");
             }
            jEditorPane.setText(jEditorPane.getText()+"**************END OF SEARCH RESULT**************\n\n\n");
         }
      
        
        
        
        if(sr.getResults()!=null && sr.getType().equals("ITEM"))
        {   
            jEditorPane.setText(jEditorPane.getText()+"Details showing of "+sr.getType()+" For matching as "+sr.getSubType()+"\n");
            for(int i=0;i<sr.getResults().size();i++)
            {
                ItemMaster cd=(ItemMaster) sr.getResults().get(i);
                jEditorPane.setText(jEditorPane.getText()
                        +(i+1)+" "+"Item Type: "+cd.getItemType()+"\n\t"
                        +"SL. No. "+cd.getItemId()+"   Model No. "+cd.getItemModelNumber()+"\n\t"
                        +"VAT: "+cd.getVat()+" %"+"\n\t"
                        +"Purchase Price: Rs. "+cd.getPurchasePrice()+"\n\t"
                        +"MRP: Rs. "+cd.getMRP()+"\n\t"
                        +"Manufacturer: "+cd.getManufacturer()+"\n");
                jEditorPane.setText(jEditorPane.getText()+"*****************************************************"+"\n\n");
                
            }
            jEditorPane.setText(jEditorPane.getText()+"**************END OF SEARCH RESULT**************\n\n\n");
            
        }
    }
    }

    public static void clear(JEditorPane jEditorPane) {
        jEditorPane.setText("");
    }
    
}
